# OS : Linux Mint 20.1
# Website : github.com

1. Create a new repository in github.com
- Copy the url of the clone link

2. Create a new directory on your computer

3. In the directory, create a new file where you write what you want
(For example : "Test.txt" where I write "Hello this is a test")

4. Open a new terminal in the directory

5. Type "git init" then enter
This command initializes the ".github" directory

6. Type "git config --global user.name YourName"
Change "YourName" for your nickname

7. Type "git config --global user.email YourEmailAddress"
Change "YourEmailAddress" for your email address

8. Type "git remote add origin URLGitRepository"
(For example : "git remote add origin https://github.com/MyAccount/MyRep.git"

9. Type "git status" to see the current progression of your changes
You can see that the file "Test.txt" is in red, we need to add it

10. Type "git add YourFile"
(For example : "git add Test.txt")

11. Now if you type "git status" you can see that the file is in green, ready to be commited

12. Type "git commit -m "YourMessage"
This will create a new commit with the added files and "YourMessage" as the commit message

13. Now you can type "git push -u origin *branch*"
Replace *branch* with the branch of your repository (master by default)
(For example : "git push -u origin master")

The file "Test.txt" is now uploaded on the GitHub website !

========================================================================================

UNDO A COMMIT ALREADY PUSHED:

1. git log --oneline
to see all commits
HEAD~0 last commit
HEAD~1 third commit
HEAD~2 second commit (we want to go back to this one)
HEAD~3 first commit


2. git reset --soft HEAD~x
where x is the number of the commit you want to go back to
git reset --soft HEAD~2

3. make your new commit, don't forget to add the changes

4. git push --force
force the new push

===================================

RESTORER UN COMMIT RESET:

1. git reflog

2. git reset --hard HEAD{x}
Where x is the number of the reset commit you want to restore.

===================================

à essayer: git config --global core.editor nano

=======================================

DELETE ALL COMMITS:

git update-ref -d HEAD