# OS : Linux Mint 20.1 (Cinnamon)
# VPN : CyberGhost (with paid subscription)

1. Go to "cyberghostvpn.com", log in and create a new "Other" device.
- Protocol : OpenVpn
- Country : *ChooseOne*
- Server Group : Premium
- Check the 4 checkboxes

2. Click on "View configuration" then "Download configuration"
(Keep this page open to have the username and the password for step 10)

3. Unzip the .tar.gz file

4. Create a new hidden directory in Home (".cyberghost" for example)

5. Paste the unzipped files in the hidden directory

6. Click on the network icon at the bottom right of the screen, then choose "network settings"

7. Click on the + button, then "Import from files"

8. Choose the ".ovpn" file in the hidden directory

9. Enter the name of the connexion
(Choose a clever name to be anonymous, related to the country you choose at step 1)

10. Enter the username and the password that you can find on the page of step 2
WARNING ! DO NOT ENTER YOUR CYBERGHOST USERNAME AND PASSWORD ACCOUNT !

11. Click on Apply, now you can start your VPN by clicking on the network icon at the bottom right of the screen !

Now let's configure the computer so that the VPN starts at startup :

12. Open terminal and type "nmcli con", then copy the line in the "UUID" column

13. Click on Menu > Preferences > Startup Applications

14. Click on the + icon, then on "Custom command" to add one
- Name : Enter the name you want ("OpenVpn" for example)
- Command : Enter "nmcli con up uuid *YourUUID*" (replace *YourUUID* with the line you got at step 12)
- Execution delay : Enter 3 seconds

15. Now click on "Add", and this is it ! Now the VPN will start 3 seconds after the startup


WARNING DISCONNECTION ISSUE:

Deactivate IPv6 on the System:
- [sudo nano /etc/default/grub] (to edit the grub config file)
- Find the line: "GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
- Change it for: "GRUB_CMDLINE_LINUX_DEFAULT="ipv6.disable=1 quiet splash"
- {Ctrl-O} + {enter} to save and {Ctrl-X} to exit
- [sudo update-grub] (to update grub)
- Restart computer

Other possibility:
Open the VPN settings in the Network Manager, go in "IPv6" tab and change "method:automatic" to "method:ignored"
(I'm not sure this is going to work, I'm testing right now)