1. First install Emacs

2. Install texlive-latex-base and texlive-full:
sudo apt-get install texlive-latex-base textlive-full

3. Install aspell-fr:
sudo apt-get install aspell-fr

4. Install pygments:
sudo apt-get install python3-pygments

5. Move all files in this folder in ~/.emacs.d

6. Start Emacs

7. Set Default Font to "Hack Regular"