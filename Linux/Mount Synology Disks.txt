##################################################################
                            RAID DISKS

apt-get install -y mdadm lvm2

Assemblate RAID disks:
mdadm -Asf && vgchange -ay

Get information of devices:
cat /proc/mdstat
lvs

//No lvs output:   /dev/${md}2
//With lvs output: /dev/${VG}/${LV}3

Get RAID infos:
sudo mdadm -D --detail /dev/md/{int}

Check partition id:
sudo lvdisplay

//RAID in:      /dev/md/{int}
//Partition in: /dev/vg{int}/lv

Mount disks:
mkdir /home/user/folder1
sudo mount /dev/vg{int}/lv /home/user/folder1

##################################################################
                           MOUNT SYNOLOGY

Get shared_folders:
smbclient -L 192.168.0.250 -U {username}

Mount disk:
mkdir /home/user/folder2
sudo mount -t cifs -o vers=2.0,username={username} //192.168.0.250/{shared_folder} /home/user/folder2

//vers = smb version

##################################################################
                            SYNC FILES

sudo rsync -aurz --progress --exclude='@*' --exclude='#*' /home/smile/folder1/* /home/smile/folder2/

//Copy from folder1 to folder2
//-u = update (Copy only modified files)
//-r = recursive
//-z = compress
//--progress = show progress bar
//--exclude = exclude files with name..

##################################################################
                            REMOVE RAID

cat /proc/mdstat
//Shows RAIDs

sudo lvremove /dev/vg1000/lv
//Removes logical volume

sudo vgremove /dev/vg1000
//Removes volume group

mdadm --stop /dev/md/2
//Stop RAID device

mdadm --remove /dev/md/2
//Remove RAID revice

mdadm --zero-superblock /dev/sdb5 /dev/sdc5
//Removes superblock on the disks

cat /proc/mdstat
//Shows RAID and check if it was succesfully removed