#!/bin/bash
echo "nvidia-fix"
echo ""
echo "Monitor1: eDP-1-1:1920x1080_60 +0+0"
echo "Monitor2: HDMI-0:1920x1080_60 +1920+0"
echo "Setting..."
nvidia-settings --assign CurrentMetaMode="eDP-1-1:1920x1080_60 +0+0 { ForceFullCompositionPipeline = On }, HDMI-0:1920x1080_60 +1920+42 { ForceFullCompositionPipeline = On }"
echo "Done."